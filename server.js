const express = require("express");
const app = express();
const port = 3000;
const connectDB = require("./config/db");
const colors = require("colors");
const errorHandler = require("./middleware/error");
const dotenv = require("dotenv");
const cors = require("cors");

// Load env vars
dotenv.config({ path: "./config/config.env" });

connectDB();
// Body parser
app.use(express.json());

// Add headers before the routes are defined
app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader("Access-Control-Allow-Origin", "http://0.0.0.0:3001");

  // Request methods you wish to allow
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );

  // Request headers you wish to allow
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader("Access-Control-Allow-Credentials", true);

  // Pass to next layer of middleware
  next();
});
//cors
app.use(cors());

const Auth = require("./routes/Auth");
const User = require("./routes/User");
const Item = require("./routes/Item");

app.use("/User", User);
app.use("/Auth", Auth);
app.use("/Item", Item);

app.use(errorHandler);

app.listen(port, console.log(`Server running on port ${port}`.yellow.bold));

// Handle unhandled promise rejections
process.on("unhandledRejection", (err, promise) => {
  console.log(`Error: ${err.message}`.red);
});
