const express = require("express");
var router = express.Router();
const { getMany, getOne, getMostViewed } = require("./../controller/Item");
const Item = require("../models/Item");
const advancedResults = require("../middleware/advancedResults");
router.route("/getMany").get(advancedResults(Item), getMany);
router.route("/getOne").get(getOne);
router.route("/getMostViewed").get(getMostViewed);
module.exports = router;
