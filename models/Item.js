const Mongoose = require("mongoose");
const TestSchema = new Mongoose.Schema({
  name: {
    type: String,
  },
  image: {
    type: String,
  },
  price: {
    type: String, 
  },
  betPrice: {
    type: String,
  },
  type: {
    type: String,
  },
});

module.exports = Mongoose.model("item", TestSchema);
