const Item = require("../models/Item");
const asyncHandler = require("../middleware/async");
const ErrorResponse = require("../utils/errorResponse");

exports.getMany = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.getOne = asyncHandler(async (req, res, next) => {
  const data = await Item.findByIdAndDelete(req.params.id);

  if (!data) {
    return next(new ErrorResponse("No Data found", 400));
  }
  res.status(200).json({
    data: data,
    success: true,
  });
});

exports.getMostViewed = asyncHandler(async (req, res, next) => {
  const data = await Item.find().sort({ viewers: -1 }).limit(10);
  if (!data) {
    return next(new ErrorResponse("No Data found", 400));
  }
  res.status(200).json({
    data: data,
    success: true,
  });
});
